import './App.css';
import NavigationBar from './components/NavigationBar';
import "./style/landingpage.css";
import Intro from './components/Intro';
import Trending from './components/Trending';
import Superhero from './components/Superhero';

function App() {
  return (
    <div>
      {/* Intro Section */}
      <div className="myBG">
        <NavigationBar />
        <Intro />
      </div>
      {/* End Of Intro */}

      {/* Trending Section */}
      <div className="trending">
        <Trending />
      </div>
      {/* End Of Trending */}
      <div className="superhero">
        <Superhero />
      </div>
    </div>
  );
}

export default App;
