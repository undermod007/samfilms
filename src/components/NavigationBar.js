import React from "react";
import { Container, Nav, Navbar, NavbarBrand, NavLink } from 'react-bootstrap';

const NavigationBar = () => {
    return (
        <Navbar variant="dark">
            <Container>
            <NavbarBrand href="/">SAMFILMS</NavbarBrand>
                <Nav>
                <NavLink href="#trending">TRENDING</NavLink>
                <NavLink href="#superhero">SUPERHERO</NavLink>
                </Nav>
            </Container>
        </Navbar>
    );
}

export default NavigationBar;